jQuery(document).ready(function($) {

	// Animations
	//-----------------------------------------------
	var delay=0, setTimeoutConst;
	if (($("[data-animation-effect]").length>0) && !Modernizr.touch) {
		$("[data-animation-effect]").each(function() {
			var item = $(this),
			animationEffect = item.attr("data-animation-effect");

			if(Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
				item.appear(function() {
					if(item.attr("data-effect-delay")) item.css("effect-delay", delay + "ms");
					setTimeout(function() {
						item.addClass('animated object-visible ' + animationEffect);

					}, item.attr("data-effect-delay"));
				}, {accX: 0, accY: -130});
			} else {
				item.addClass('object-visible');
			}
		});
	};

	// PieChart
	//-----------------------------------------------
	$(".progress-pie-chart").each(function() {
		var $ppc = $(this),
		percent = parseInt($ppc.data('percent')),
		deg = 360*percent/100;
		if (percent > 99) {
			$ppc.addClass('gt-50');
		}
		$ppc.find('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
		$ppc.find('.ppc-percents span.percent').html(percent+'%');
	});


	// Map highlights
	//-----------------------------------------------
	$("#map_menu ul li").hover(
		function() {
			var target = $(this).attr('data-edtarget');
			$(".map_img").addClass(target);
		}, function() {
			var target = $(this).attr('data-edtarget');
			$(".map_img").removeClass(target);
		}
	)

	// Quiz start
	//-----------------------------------------------
	$("[data-item='question']").not("[data-index='1']").hide();

	// Sections start
	//-----------------------------------------------
	$("[data-item='section']").not("[data-index='1']").hide();

	// Select answer
	//-----------------------------------------------
	$(".answer").click(function() {
		var me = $(this);
		var correct = me.attr('data-correct');

		if (correct == "true") {
			me.addClass('selected unable');
			me.siblings('.answer').addClass('unable');
			me.parent().next().find('.nextBtn').removeClass('disabled')
			setTimeout(function(){
				changeQuestion('next');
			},400)
		} else if (correct == "false") {
		}
	});

	$( ".float-notification" ).draggable({
		cursor: "pointer",
		axis:"y",

	});

	// Drag&Drop
	//-----------------------------------------------
	$( ".draggable" ).draggable({
		revert: "invalid",
		scroll: false,
		start: function() {
			var dropTarget = $(this).data("dropped");

			console.log(dropTarget)

			$(dropTarget).data("draggable", "");
		}
	}).data("left",$(".draggable").position().left).data("top",$(".draggable").position().top);;
	
	$( ".droppable" ).droppable({
		accept: function(draggable) {
			return draggable.hasClass("draggable");
		},
		drop: function(event, ui) {
			var parent = $(this).closest('[data-item="dragndrop"]');
			var total = $(this).closest('[data-item="dragndrop"]').attr('data-total');
			var filled = 0;

			if ($(this).data("draggable")) {
				revertDraggables($(this).data("draggable"));
				$(this).data("draggable", "");
			}
			$(this).data("draggable", ".draggable[data-drag='"+ui.draggable.attr('data-drag')+"']");
			$(this).attr('data-dropped', ui.draggable.attr('data-drag'));
			ui.draggable.position({of:this, my:"center", at:"center"});
			ui.draggable.data("dropped", ".droppable[data-drop='"+$(this).attr('data-drop')+"']");

			$(this).parent('.drops').children('.droppable').each(function() {
				if ($(this).attr('data-dropped').length)
				filled++;
			});

			console.log(total, filled)
			
			if (total == filled) {
				parent.siblings('.checkDragndrop').removeClass('inactive')
			}
		}
	}).data('draggable', '').attr('data-dropped', '');

	//////////

	$(".puzzle-drag").draggable({
		revert: "invalid",
		scroll: false,
		start: function() {
			var dropTarget = $(this).data("dropped");

			$(dropTarget).data("draggable", "");
		}
	}).data("left",$(".puzzle-drag").position().left).data("top",$(".puzzle-drag").position().top);

	$(".puzzle-drop").droppable({
		accept: function(draggable) {
			return draggable.hasClass("puzzle-drag");
		},
		drop: function(event, ui) {
			var parent = $(this).closest('[data-item="dragndrop"]');
			var total = $(this).closest('[data-item="dragndrop"]').attr('data-total');
			var filled = 0;

			if ($(this).data("draggable")) {
				revertDraggables($(this).data("draggable"));
				$(this).data("draggable", "");
			}
			$(this).data("draggable", ".puzzle-drag[data-drag='"+ui.draggable.attr('data-drag')+"']");
			$(this).attr('data-dropped', ui.draggable.attr('data-drag'));
			ui.draggable.position({of:this, my:"center", at:"center"});
			ui.draggable.data("dropped", ".puzzle-drop[data-drop='"+$(this).attr('data-drop')+"']");

			$(this).parent('.drops').children('.puzzle-drop').each(function() {
				if ($(this).attr('data-dropped').length)
				filled++;
			});

			console.log(total, filled)
			
			if (total == filled) {
				parent.siblings('.checkPuzzle').removeClass('inactive')
			}
		}
	}).data("draggable","").attr('data-dropped', '');

	$(".drags").on('click', '.dropped', function() {
		revertDraggables($(this));
	});
	
});

function revertDraggables(selector) {
	$(selector).animate({"left":$(selector).data("left"), "top":$(selector).data("top")});
}

// Change stage
//-----------------------------------------------
function changeSection(key) {
	var actual = $("[data-item='section'].active");
	var actualKey = actual.attr('data-index');
	var total = $("[data-item='section']").length;

	if (key == "next") {
		var next = actual.next("[data-item='section']");
		var nextKey = next.attr('data-index');

		$("[data-item='section'][data-key='prev']").removeClass('disabled');
		actual.removeClass('active bounceIn').addClass('animated bounceOut transition');
		next.addClass('animated transition').removeClass('bounceOut bounceOut').show();
		setTimeout(function() {
			actual.hide().removeClass('transition');
		}, 1000);
		next.addClass('active').removeClass('transition');


	} else if (key == "prev") {
		var prev = actual.prev("[data-item='section']");
		var prevKey = prev.attr('data-index');

		$("[data-item='section'][data-key='next']").removeClass('disabled');
		actual.removeClass('active bounceIn').addClass('animated bounceOut transition');
		prev.addClass('animated bounceIn transition').removeClass('bounceOut').show();
		setTimeout(function() {
			actual.hide().removeClass('transition');
		}, 1000);
		prev.addClass('active').removeClass('transition');
	}
}

// Change question
//-----------------------------------------------
function changeQuestion(key) {
	var actual = $("[data-item='question'].active");
	var actualKey = actual.attr('data-index');
	var total = $("[data-item='question']").length;

	$(".float-notification").addClass('animated bounceOutLeft').removeClass('bounceInLeft');
	setTimeout(function() {
		$(".float-notification").hide();
	}, 400);

	if (key == "next") {
		var next = actual.next("[data-item='question']");
		var nextKey = next.attr('data-index');

		if (nextKey == total) {
			$("[data-item='questionBut'][data-key='next']").addClass('disabled');
		} else {
			$("[data-item='questionBut'][data-key='next']").removeClass('disabled');
		}
		$("[data-item='questionBut'][data-key='prev']").removeClass('disabled');
		actual.removeClass('active bounceInRight').addClass('animated bounceOutLeft transition');
		next.addClass('animated bounceInRight transition').removeClass('bounceOutLeft bounceOutRight').show();
		$(".step[data-key='"+actualKey+"']").removeClass('active');
		$(".step[data-key='"+nextKey+"']").addClass('active');
		setTimeout(function() {
			actual.hide().removeClass('transition');
		}, 1000);
		next.addClass('active').removeClass('transition');
		
		var walk = $("[data-item='questionBut'][data-key='next']").hasClass('walkthrough');
		if (walk == true) {
			next.find('video')[0].currentTime = 0;
			next.find('video')[0].play();
		}

	} else if (key == "prev") {
		var prev = actual.prev("[data-item='question']");
		var prevKey = prev.attr('data-index');

		if (actualKey == 1 || prevKey == 1) {
			$("[data-item='questionBut'][data-key='prev']").addClass('disabled');
		} else {
			
		}
		$("[data-item='questionBut'][data-key='next']").removeClass('disabled');
		actual.removeClass('active bounceInRight').addClass('animated bounceOutRight transition');
		prev.addClass('animated bounceInLeft transition').removeClass('bounceOutLeft').show();
		$(".step[data-key='"+actualKey+"']").removeClass('active');
		$(".step[data-key='"+prevKey+"']").addClass('active');
		setTimeout(function() {
			actual.hide().removeClass('transition');
		}, 1000);
		prev.addClass('active').removeClass('transition');

		var walk = $("[data-item='questionBut'][data-key='prev']").hasClass('walkthrough');
		if (walk == true) {
			prev.find('video')[0].currentTime = 0;
			prev.find('video')[0].play();
		}
	}
}

// Toggle Target
//-----------------------------------------------
function toggleTarget(target) {
	var el = $("[data-item='"+target+"'");
	var state = el.attr('data-state');

	if (state == "inactive" ) {
		$(".menu-button").toggleClass('icon-menu icon-cross');
		el.removeClass('slideOutRight').addClass('animated slideInRight').attr('data-state', 'active');
		$("#content").append('<div class="overlay animated fadeIn"></div>')
	} else if (state == "active") {
		$(".menu-button").toggleClass('icon-menu icon-cross');
		el.removeClass('slideInRight').addClass('animated slideOutRight');
		$("#content .overlay").addClass('animated fadeOut');
		setTimeout(function(){
			el.attr('data-state', 'inactive');
			$("#content .overlay").remove();
		}, 300);
	}

}

// Select answer
//-----------------------------------------------
function selectAnswer(elem) {
	var me = $(elem);

	me.siblings('.answer').removeClass('selected');
	me.addClass('selected');
	me.siblings('.checkAnswer').removeClass('inactive')
}

// Check answer
//-----------------------------------------------
function checkAnswer(elem) {
	var me = $(elem);
	var selected = me.siblings('.answer.selected').attr('data-key');

	if (selected == "1") {
		$(".float-notification.right").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	} else {
		$(".float-notification.wrong").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	}
}

// Check select
//-----------------------------------------------
function checkSelect(elem) {
	var me = $(elem);
	var total = me.siblings().find('select').length;
	var right = 0;

	me.siblings().find('select').each(function() {
		var key = $(this).val();
		if (key == 1) {
			$(this).parent('label').addClass('right');
			right++;
		} else {
			$(this).parent('label').addClass('wrong');
		}
	});
	if (right == total) {
		$(".float-notification.right").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	} else {
		$(".float-notification.wrong").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	}
}

// Check dragndrop
//-----------------------------------------------
function checkDragndrop(elem) {
	var me = $(elem);
	var drops = me.siblings('[data-item="dragndrop"]').find('.droppable');
	var drags = me.siblings('[data-item="dragndrop"]').find('.draggable');
	var total = drops.length;
	var totalRight = 0;

	drops.each(function() {
		var dropkey = $(this).attr('data-drop');
		var droppedkey = $(this).attr('data-dropped');

		console.log(drops, dropkey, droppedkey)


		if (dropkey == droppedkey) {
			$(this).parent().siblings('.drags').find("[data-drag='"+droppedkey+"']").addClass('right');
			totalRight++;
		} else {
			$(this).parent().siblings('.drags').find("[data-drag='"+droppedkey+"']").addClass('wrong');
		}

		console.log(total, totalRight)

	});

	if (totalRight == total) {
		$(".float-notification.right").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	} else {
		$(".float-notification.wrong").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	}
	drags.draggable('disable');
}

// Check puzzle
//-----------------------------------------------
function checkPuzzle(elem) {
	var me = $(elem);
	var drops = me.siblings('[data-item="dragndrop"]').find('.puzzle-drop');
	var drags = me.siblings('[data-item="dragndrop"]').find('.puzzle-drag');

	var total = drops.length;
	var totalRight = 0;

	drops.each(function() {
		var dropkey = $(this).data('drop');
		var droppedkey = $(this).data('dropped');

		console.log(drops, dropkey, droppedkey)

		if (dropkey == droppedkey) {
			$(this).parent().siblings('.drags').find("[data-drag='"+droppedkey+"']").addClass('right');
			totalRight++;
		} else {
			$(this).parent().siblings('.drags').find("[data-drag='"+droppedkey+"']").addClass('wrong');
			
		}

	});
	if (totalRight == total) {
		$(".float-notification.right").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	} else {
		$(".float-notification.wrong").addClass('animated bounceInLeft').removeClass('bounceOutLeft').show();
		me.html("CONTINUAR <span class='icon-arrow-right-circle'></span>").attr('onClick', 'changeQuestion("next")');
	}
	drags.draggable('disable');
}